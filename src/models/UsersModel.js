const { DataTypes } = require('sequelize');
const { Database } = require('../libraries/database');

function UsersModel() {
    return Database.define('Users', {
        id: {
            type: DataTypes.NUMBER, // character variying , string
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        fullname: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        role: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        tableName: 'tb_user',
        timestamps: true,
    })
}

module.exports = UsersModel;
