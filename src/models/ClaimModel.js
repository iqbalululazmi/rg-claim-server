const { DataTypes } = require('sequelize');
const { Database } = require('../libraries/database');

function ClaimModel() {
  return Database.define(
    'Claim',
    {
      id: {
        type: DataTypes.NUMBER, // character variying , string
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      userPhoneNumber: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      packageTag: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      packageName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      packageSerial: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      orderStatus: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      city: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      province: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      postalCode: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      prize: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      tableName: 'tb_claim',
      timestamps: true,
    }
  );
}

module.exports = ClaimModel;
