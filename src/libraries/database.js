const Sequelize = require('sequelize');
var conString = 'pg://admin:guest@localhost:5432/Employees';

const Database = new Sequelize({
  username: 'uiejjsqvxmwsye',
  password: 'f9e2ab2137535c336a378edb91f7a27dd14262a8a43ef7342f41a77ebf8c516b',
  database: 'd4rifq24cg1l2j',
  host: 'ec2-34-228-100-83.compute-1.amazonaws.com',
  dialect: 'postgres',
  ssl: true,
  protocol: 'postgres',
  logging: true,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false, // <<<<<< YOU NEED THIS
    },
  },
});

// check database connections
async function checkConnection() {
  console.log('trying to connect postgre database...');
  try {
    await Database.authenticate();
    console.log('Database connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database ', error);
    throw error;
  }
}

module.exports = {
  Database,
  checkConnection,
};
