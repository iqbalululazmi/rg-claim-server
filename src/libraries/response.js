function makeResponse(res, { data }) {
  res.status(200).json({
    code: 200,
    status: 'success',
    data,
  });
  return;
}

function makeErrorResponse(res, { message }) {
  res.status(403).json({
    code: 403,
    status: 'error',
    message,
  });
  return;
}

module.exports = { makeResponse, makeErrorResponse };
