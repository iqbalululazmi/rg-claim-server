const ClaimModel = require('../../models/ClaimModel');
const Password = require('../../libraries/password');
const { makeErrorResponse } = require('../../libraries/response');
const moment = require('moment');

class ClaimController {
  async index(req, res) {
    try {
      const claimModel = ClaimModel();
      const data = await claimModel.findAll();
      return res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }

  async create(req, res) {
    try {
      const payload = req.body;
      const claimModel = ClaimModel();
      const packages = payload.packages;

      if (packages.length === 0) {
        return makeErrorResponse(res, {
          message: 'Package cannot be empty',
        });
      }

      console.log(payload);
      try {
        await Promise.all(
          packages.forEach(async (pack) => {
            const param = {
              userId: payload.userId,
              userPhoneNumber: payload.userPhoneNumber,
              address: payload.address,
              city: payload.city,
              province: payload.province,
              postalCode: payload.postalCode,
              orderStatus: pack.orderStatus,
              packageName: pack.packageName,
              packageSerial: pack.packageSerial,
              packageTag: pack.packageTag,
              prize: pack.prize,
              status: 'created',
            };
            await claimModel.create(param);
          })
        );

        return res.status(201).json({
          data: 'successfully submit',
        });
      } catch (error) {
        return makeErrorResponse(res, {
          message: error.toString(),
        });
      }
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }

  async update(req, res) {
    try {
      const id = req.params.userID;
      const payload = req.body;
      const claimModel = ClaimModel();
      const response = await claimModel.update(
        {
          username: payload.username,
          email: payload.email,
          password: payload.password,
          fullName: payload.fullname,
          role: payload.role,
        },
        {
          where: {
            id,
          },
        }
      );
      return res.status(201).json({
        data: response,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }

  async updateStatus(req, res) {
    try {
      const userId = req.params.userId;
      const payload = req.body;
      const claimModel = ClaimModel();
      const response = await claimModel.update(
        {
          status: payload.status,
          updatedAt: moment(),
        },
        {
          where: {
            userId,
          },
        }
      );
      return res.status(201).json({
        message: 'Successfully updated status',
      });
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }

  async remove(req, res) {
    try {
      const id = req.params.userID;
      const claimModel = ClaimModel();
      const response = await claimModel.destroy({
        where: {
          id,
        },
      });
      return res.status(200).json({
        data: response,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }

  async getByUserId(req, res) {
    try {
      const userId = req.params.userId;
      const claimModel = ClaimModel();
      const data = await claimModel.findOne({
        where: {
          userId,
        },
      });
      return res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        error: error.toString(),
      });
    }
  }
}

module.exports = new ClaimController();
