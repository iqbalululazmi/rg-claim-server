const { makeResponse, makeErrorResponse } = require('../../libraries/response');
const UserModel = require('../../models/UsersModel');
const Password = require('../../libraries/password');
const Token = require('../../libraries/token');
class AuthController {
  async login(req, res) {
    try {
      const payload = req.body;
      console.log(payload);
      const userModel = await UserModel();
      const data = await userModel.findOne({
        where: {
          username: payload.username,
        },
      });
      if (data) {
        if (Password.matchPassword(req.body.password, data.password)) {
          // create access token
          const token = Token.sign({
            id: data.id,
            username: data.username,
            email: data.email,
            fullname: data.fullname,
            role: data.role,
          });

          return makeResponse(res, {
            data: {
              accessToken: token,
            },
          });
        }
        return makeErrorResponse(res, {
          message: 'Username and Password is failed',
          data: {},
        });
      }

      return makeErrorResponse(res, {
        message: 'username and password are wrong',
      });
    } catch (error) {
      console.log(error);
      return makeErrorResponse(res, {
        message: 'Failed Authenticated',
        data: {},
      });
    }
  }
}

module.exports = new AuthController();
