const UserModel = require('../../models/UsersModel');
const Password = require('../../libraries/password');

class UsersController {
    async index(req, res) {
        try {
            const userModel = UserModel();
            const data = await userModel.findAll();
            return res.status(200).json({
                data,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async create(req, res) {
        try {
            const payload = req.body;
            const userModel = UserModel();
            const pas = await Password.makePassword(payload.password)
            console.log(pas)
            const response = await userModel.create({
                username: payload.username,
                email: payload.email,
                password: pas,
                fullname: payload.fullname,
                role: payload.role
            });
            return res.status(201).json({
                data: response,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async update(req, res) {
        try {
            const id = req.params.userID;
            const payload = req.body;
            const userModel = UserModel();
            const response = await userModel.update({
                username: payload.username,
                email: payload.email,
                password: payload.password,
                fullName: payload.fullname,
                role: payload.role,
            }, {
                where: {
                    id,
                },
            });
            return res.status(201).json({
                data: response,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async remove(req, res) {
        try {
            const id = req.params.userID;
            const userModel = UserModel();
            const response = await userModel.destroy({
                where: {
                    id,
                }
            });
            return res.status(200).json({
                data: response,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async getByUserID(req, res) {
        try {
            const id = req.params.userID;
            const userModel = UserModel();
            const data = await userModel.findOne({
                where: {
                    id,
                }
            });
            return res.status(200).json({
                data,
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }
}

module.exports = new UsersController();
