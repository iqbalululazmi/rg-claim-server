const jwt = require('jsonwebtoken');
const UserModel = require('../../models/UsersModel');
const { makeResponse, makeErrorResponse } = require('../../libraries/response');

class ProfileController {
  async profile(req, res) {
    try {
      const userModel = UserModel();
      const user = req.user;
      console.log(user);

      const data = await userModel.findOne({
        where: {
          id: user.id,
        },
        attributes: {
          exclude: ['password'], // field password tidak perlu ditampilkan pada response
        },
      });

      makeResponse(res, {
        data,
      });
    } catch (error) {
      console.log(error);
      makeErrorResponse(req, {
        message: 'Failed get user profile',
      });
    }
  }
}

module.exports = new ProfileController();
