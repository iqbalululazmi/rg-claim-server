const express = require('express');

const UsersController = require('../controllers/api/UsersController');
const ClaimController = require('../controllers/api/ClaimController');
const AuthController = require('../controllers/api/AuthController');
const ProfileController = require('../controllers/api/ProfileController');
const { validateToken } = require('../middleware/api/auth');

function routes() {
  const router = express.Router();

  router.get('/user', function (req, res) {
    UsersController.index(req, res);
  });

  router.post('/user', function (req, res) {
    UsersController.create(req, res);
  });

  router.put('/user/:userID', function (req, res) {
    UsersController.update(req, res);
  });

  router.delete('/user/:userID', function (req, res) {
    UsersController.remove(req, res);
  });

  router.get('/user/:userID', function (req, res) {
    UsersController.getByUserID(req, res);
  });

  router.post('/auth/login', function (req, res) {
    AuthController.login(req, res);
  });

  router.get('/profile', validateToken, function (req, res) {
    ProfileController.profile(req, res);
  });

  router.get('/testing', function (req, res) {
    res.status(200).json({
      message: 'Hello Heroku!',
    });
  });

  router.get('/claim', function (req, res) {
    ClaimController.index(req, res);
  });

  router.post('/claim', function (req, res) {
    ClaimController.create(req, res);
  });

  router.put('/claim/:claimID', function (req, res) {
    ClaimController.update(req, res);
  });
  
  router.put('/claimStatus/:userId', function (req, res) {
    ClaimController.updateStatus(req, res);
  });

  router.delete('/claim/:claimID', function (req, res) {
    ClaimController.remove(req, res);
  });

  router.get('/claim/:userId', function (req, res) {
    ClaimController.getByUserId(req, res);
  });

  router.get('/testing', function (req, res) {
    res.status(200).json({
        message: 'Hello Heroku!',
    });
});


  return router;
}

module.exports = routes;
